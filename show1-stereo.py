import time
import cv2
import cv2.aruco
import numpy as np
import time
import traceback
import sys

sidecalibs = {}
cameras = []

import pickle
for side, damnum in zip(("left", "right"),(0,2)):
	with open(side, 'rb') as f:
		retval, cameraMatrix, distCoeffs, rvecs, tvecs = pickle.load(f)
		sidecalibs[side] = (retval, cameraMatrix, distCoeffs, rvecs, tvecs)
	#Start capturing images for calibration
	cap = cv2.VideoCapture(damnum, cv2.CAP_V4L2)
	cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
	cap.set(cv2.CAP_PROP_FPS,350)
	cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 3.0) 
	# 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
	ret,frame = cap.read()
	print(ret)
	if ret == True:
		cameras.append(cap)


while True:
	imgs = []
	for idx, cap in enumerate(cameras):
		ret,frame = cap.read()
		#gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		if ret:
			imgs.append(cv2.undistort(frame, cameraMatrix, distCoeffs))
		else:
			traceback.print_exc(file=sys.stdout)
	sf = 0.65
	shw = cv2.resize(np.vstack(imgs), (0, 0), fx = sf, fy = sf)
	cv2.imshow(f"frame",shw)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
	time.sleep(0.05)
	

cap.release()
cv2.destroyAllWindows()