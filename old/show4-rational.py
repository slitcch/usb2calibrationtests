import time
import cv2
import cv2.aruco
import numpy as np
import time
import traceback
import sys

np.set_printoptions(suppress = True, precision=8)

sidecalibs = {}
cameras = []

cv2.namedWindow("frame",0) # 0 at end makes it resizeable in Python 

calibration = np.load("cameraCalibration_cv2.npz")
# pts = np.array([[[1280./2,  720./2]],

#  [[0,  0]],

#  [[1280,  720]],

# 	], dtype=np.float)
# print(cv2.undistortPoints(pts, calibration['leftCameraMatrix'], calibration["leftDistCoeffs"], R=calibration['R1']) )

for damnum in [1,3]:
	#Start capturing images for calibration
	cap = cv2.VideoCapture(damnum, cv2.CAP_V4L2)
	cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
	cap.set(cv2.CAP_PROP_FPS,350)
	cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 3.0) 
	# 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
	ret,frame = cap.read()
	print(ret, damnum)
	if ret == True:
		cameras.append(cap)

pts = [[0,0.5,1,1.5],
       [0,0.25,.5,.75]]


cameraMatrixLeft, roi=cv2.getOptimalNewCameraMatrix(calibration['leftCameraMatrix'],calibration['leftDistCoeffs'],(1280,720),1,(1280,720))

cameraMatrixRight, roi=cv2.getOptimalNewCameraMatrix(calibration['rightCameraMatrix'],calibration['rightDistCoeffs'],(1280,720),1,(1280,720))

while True:
	imgs = []
	for idx, cap in enumerate(cameras):
		ret,frame = cap.read()
		# frame = frame[:, 280:1000,:]
		if idx == 0:
			cameraMatrix = cameraMatrixLeft
			distCoeffs = calibration['leftDistCoeffs']
		else:
			cameraMatrix = cameraMatrixRight
			distCoeffs = calibration['rightDistCoeffs']
		if ret:
			imgs.append(cv2.undistort(frame, cameraMatrix, distCoeffs))
		else:
			traceback.print_exc(file=sys.stdout)
	sf = 1
	shw = cv2.resize(np.hstack(imgs), (0, 0), fx = sf, fy = sf)
	cv2.imshow(f"frame",shw)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
	

cap.release()
cv2.destroyAllWindows()