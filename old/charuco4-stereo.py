import time
import cv2
import cv2.aruco
import numpy as np
import random
import multiprocessing
import traceback
import sys

import pickle

# squaresize = .0551
# squaresize = .05178
squaresize = 0.04827
arucosize = squaresize*.8

checkerboardWidth = 11
checkerboardHeight = 7

monitorWidthPx = 2560
monitorHeightPx = 1440
bine = 15

numImagesRequired = (checkerboardWidth-1)*(checkerboardHeight-1)

dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_100)
board = cv2.aruco.CharucoBoard_create(checkerboardWidth,checkerboardHeight,squaresize,arucosize,dictionary)
img = board.draw((monitorWidthPx,monitorHeightPx-(2*bine)))

img = np.vstack((np.ones((bine,monitorWidthPx)), img, np.ones((bine,monitorWidthPx))))



#Dump the calibration board to a file

cv2.namedWindow("oh yeah", 0)
cv2.moveWindow("oh yeah", 2200, 100)
cv2.setWindowProperty("oh yeah", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

cv2.imshow('oh yeah',img)

#Start capturing images for calibration
cameras = []

for num in [2,0]:
	cap = cv2.VideoCapture(num, cv2.CAP_V4L2)
	cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
	cap.set(cv2.CAP_PROP_FPS,350)
	cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1.0) # 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
	cap.set(cv2.CAP_PROP_EXPOSURE, 60)
	ret,frame = cap.read()
	print(num,ret)
	if ret == True:
		cameras.append(cap)


allCorners = [[],[]]
allIds = [[],[]]

allCornersStereo = [[],[]]

eriam = 0
doAppend = False
while True:
	imgs = []
	for idx, cap in enumerate(cameras):
		ret,frame = cap.read()
		frame = frame[:, 280:1000,:]
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		res = cv2.aruco.detectMarkers(gray,dictionary)
		mcorners = res[0]
		ids = res[1]
		currentCorners = [[],[]]
		currentIds = [[],[]]
		if len(res[0])>0:
			e = None
			retval, currentCorners[idx], currentIds[idx] = cv2.aruco.interpolateCornersCharuco(mcorners, ids ,gray,board,)
			cv2.aruco.drawDetectedMarkers(frame,res[0],res[1])
			try:
				cv2.aruco.drawDetectedCornersCharuco(frame, currentCorners[idx], currentIds[idx])
			except:
				print("nope")
			if doAppend and currentCorners[idx] is not None and currentCorners[idx] is not None and len(currentCorners[idx])>3:
				print(f"capturing {idx}")
				allCorners[idx].append(currentCorners[idx])
				allIds[idx].append(currentIds[idx])
		imgs.append(frame)
	try:
		if doAppend and (len(allCorners[0][-1]) == numImagesRequired) and (len(allCorners[1][-1]) == numImagesRequired):
			print(f"capturing stereo {len(allCornersStereo[0])+1}")
			eriam+=1
			print(eriam)
			allCornersStereo[0].append(allCorners[0][-1])
			allCornersStereo[1].append(allCorners[1][-1])
	except:
		traceback.print_exc(file=sys.stdout)
	sf = 1
	shw = cv2.resize(np.hstack(imgs), (0, 0), fx = sf, fy = sf)
	cv2.imshow(f"frame",shw)
	ert = cv2.waitKey(1)
	if ert & 0xFF == ord('q'):
		break
	doAppend = (ert & 0xFF == ord("e"))


	

imsize = gray.shape

cap.release()
cv2.destroyAllWindows()



for n in range(2):
	idsreee = allIds[n]
	cornersreee = allCorners[n]
	l = len(idsreee)
	r = list(range(len(idsreee)))

	random.shuffle(r)

	newAllCorners = []
	newAllIds = []

	for ele in r:
		newAllCorners.append(cornersreee[ele])
		newAllIds.append(idsreee[ele])

	print("number of calibs:", l)

	allCorners[n] = newAllCorners[:min(numImagesRequired,l)]
	allIds[n] = newAllIds[:min(numImagesRequired,l)]

	print("now it's just", len(allCorners[n]), len(allIds[n]))

def wrape(allCorners,allIds,board,imsize, ye, filename):
	# calibrateCameraCharuco has some assert()s in it, I think as sanity checks to make sure the math hasn't gone horribly wrong.
	# These asserts will raise an exception though, so catch it
	try:
		retval, cameraMatrix, distCoeffs, rvecs, tvecs = cv2.aruco.calibrateCameraCharuco(allCorners,allIds,board,imsize,None,None)
		yeah[filename] = {"cameraMatrix": cameraMatrix, "distCoeffs": distCoeffs, "rvecs": rvecs, "tvecs": tvecs}
		# with open(filename, 'wb') as f:
		# 	# Pickle the 'data' dictionary using the highest protocol available.
		# 	pickle.dump((retval, cameraMatrix, distCoeffs, rvecs, tvecs), f, pickle.HIGHEST_PROTOCOL)

	except:
		traceback.print_exc(file=sys.stdout)
		print("oh no", filename)


print("okay, calbrating....")
nobs = []
man = multiprocessing.Manager()
yeah = man.dict()



nobs.append(multiprocessing.Process(target=wrape, args=(allCorners[0],allIds[0],board,imsize, yeah, "left")))
nobs.append(multiprocessing.Process(target=wrape, args=(allCorners[1],allIds[1],board,imsize, yeah, "right")))



for job in nobs:
	job.start()

jobs = nobs

for nob in jobs:
	nob.join()

print(len(allCorners[0]))

# for idx, ele in enumerate(allCorners[0]):


# for l in (0,1):
# 	l

# print("l", len(allCornersStereo[0]), len(allCornersStereo[1] ))
# random.shuffle(allCornersStereo[0])
# random.shuffle(allCornersStereo[1])
# allCornersStereo = [allCornersStereo[0][:numImagesRequired], allCornersStereo[1][:numImagesRequired]]
# print("l", len(allCornersStereo[0]), len(allCornersStereo[1] ))

print("[]", len(allCornersStereo[0][0]))
print("eram", len(allCornersStereo[0]))
# Chessboard parameters
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ...., (checkerboardWidth, checkerboardHeight,0)
objpp = np.zeros(((checkerboardHeight-1)*(checkerboardWidth-1),3), np.float32)
objpp[:,:2] = np.mgrid[0:checkerboardWidth-1,0:checkerboardHeight-1].T.reshape(-1,2)
objpp = objpp * squaresize # Set the Object Points to be in real coordinates
objpp = np.asarray([objpp])
objp = np.copy(objpp)
for x in range(len(allCornersStereo[0])-1):
	objp = np.concatenate((objp, objpp), axis=0)
# print(objp)

# objp = objp
# objp = objp[:len(allCornersStereo[0])]
print("hello", len(objp))

valid, leftCameraMatrix, leftDistCoeffs, rightCameraMatrix, rightDistCoeffs, leftToRightRot, leftToRightTrans, essentialMat, fundamentalMat = (
 							cv2.stereoCalibrate(objp, allCornersStereo[0], allCornersStereo[1], yeah["left"]["cameraMatrix"], yeah["left"]["distCoeffs"], yeah["right"]["cameraMatrix"], yeah["right"]["distCoeffs"], imsize))

if(valid):
	# Construct the stereo-rectified parameters for display
	R1, R2, P1, P2, Q, validPixROI1, validPixROI2 = cv2.stereoRectify(leftCameraMatrix,  leftDistCoeffs, 
																	  rightCameraMatrix, rightDistCoeffs, 
																	 imsize, 
																	  leftToRightRot, leftToRightTrans)

	leftUndistortMap = [None, None]
	leftUndistortMap[0], leftUndistortMap[1] = cv2.initUndistortRectifyMap(leftCameraMatrix, leftDistCoeffs, 
																		   R1, P1, imsize, cv2.CV_32FC1)
	rightUndistortMap = [None, None]
	rightUndistortMap[0], rightUndistortMap[1] = cv2.initUndistortRectifyMap(rightCameraMatrix, rightDistCoeffs, 
																			 R2, P2, imsize, cv2.CV_32FC1)
	undistortMap = (leftUndistortMap, rightUndistortMap)

	print("Stereo Calibration Completed!")
	print("Left to Right Rotation Matrix:")
	print(leftToRightRot)
	print("Left to Right Translation:")
	print(leftToRightTrans)
	print("Essential Matrix:")
	print(essentialMat)
	print("Fundamental Matrix:")
	print(fundamentalMat)
	np.savez("cameraCalibration_cv2.npz",
			leftCameraMatrix=leftCameraMatrix,
			rightCameraMatrix=rightCameraMatrix,
			leftDistCoeffs=leftDistCoeffs,
			rightDistCoeffs=rightDistCoeffs,
			leftToRightTrans=leftToRightTrans,
			leftToRightRot=leftToRightRot,
			R1=R1,
			R2=R2,
			P1=P1,
			P2=P2,
			baseline=float(leftToRightTrans[0]*-1.0)
			)
	import datetime
	import shutil
	timea = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")
	shutil.copy("cameraCalibration_cv2.npz", f"cameraCalibration_cv2-{timea}.npz")


	
