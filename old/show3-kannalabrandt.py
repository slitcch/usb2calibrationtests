import time
import cv2
import cv2.aruco
import numpy as np
import time
import traceback
import sys

sidecalibs = {}
cameras = []

calibration = np.load("cameraCalibration_cv2.npz")

for damnum in (0,2):
	#Start capturing images for calibration
	cap = cv2.VideoCapture(damnum, cv2.CAP_V4L2)
	cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
	cap.set(cv2.CAP_PROP_FPS,350)
	cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 3.0) 
	# 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
	ret,frame = cap.read()
	print(ret, damnum)
	if ret == True:
		cameras.append(cap)

pts = [[0,0.5,1,1.5],
       [0,0.25,.5,.75]]

K_left = calibration['leftCameraMatrix']
D_left = calibration['leftDistCoeffs']

K_right = calibration['rightCameraMatrix']
D_right = calibration['rightDistCoeffs']

P_left = calibration['P1'] #cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D, dim2, np.eye(3), balance=balance)

P_right = calibration['P2']

R_right = calibration['R1']
R_left = calibration['R2']

(lm1, lm2) = cv2.fisheye.initUndistortRectifyMap(K_left, D_left, R_left, P_left, (1280,720), cv2.CV_32FC1)
(rm1, rm2) = cv2.fisheye.initUndistortRectifyMap(K_right, D_right, R_right, P_right, (1280,720), cv2.CV_32FC1)
undistort_rectify = {"left"  : (lm1, lm2),
                         "right" : (rm1, rm2)}


while True:
	imgs = []
	for idx, cap in enumerate(cameras):
		ret,frame = cap.read()
		if idx == 0:
			cameraMatrix = calibration['leftCameraMatrix']
			distCoeffs = calibration['leftDistCoeffs']
			ur = undistort_rectify["left"]
		else:
			ur = undistort_rectify["right"]
			cameraMatrix = calibration['rightCameraMatrix']
			distCoeffs = calibration['rightDistCoeffs']

		if ret:
			imgs.append(cv2.remap(src = frame,
                                          map1 = ur[0],
                                          map2 = ur[1],
                                          interpolation = cv2.INTER_LINEAR),)
		else:
			traceback.print_exc(file=sys.stdout)
	sf = 0.65
	shw = cv2.resize(np.vstack(imgs), (0, 0), fx = sf, fy = sf)
	cv2.imshow(f"frame",shw)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
	

cap.release()
cv2.destroyAllWindows()