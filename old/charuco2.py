import time
import cv2
import cv2.aruco
import numpy as np
import random

import pickle

squaresize = .0551
arucosize = squaresize*.8

checkerboardWidth = 9
checkerboardHeight = 5

dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_50)
board = cv2.aruco.CharucoBoard_create(checkerboardWidth,checkerboardHeight,squaresize,arucosize,dictionary)
img = board.draw((2560,1440-200))

img = np.vstack((np.ones((100,2560)), img, np.ones((100,2560))))

#Dump the calibration board to a file

cv2.namedWindow("oh yeah", 0)
cv2.moveWindow("oh yeah", 2200, 100)
cv2.setWindowProperty("oh yeah", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

cv2.imshow('oh yeah',img)

#Start capturing images for calibration
cap = cv2.VideoCapture(0, cv2.CAP_V4L2)
cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
cap.set(cv2.CAP_PROP_FPS,350)
cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1.0) # 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
cap.set(cv2.CAP_PROP_EXPOSURE, 62)


allCorners = []
allIds = []
decimator = 0
while True:
		ret,frame = cap.read()
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		res = cv2.aruco.detectMarkers(gray,dictionary)
		mcorners = res[0]
		ids = res[1]
		if len(res[0])>0:
				e = None
				print(ids)
				retval, rightCorners, rightIds = cv2.aruco.interpolateCornersCharuco(mcorners, ids,gray,board,)
				cv2.aruco.drawDetectedMarkers(frame,res[0],res[1])
				try:
					# if len(rightCorners) == (checkerboardWidth-1) * (checkerboardHeight-1):
					cv2.aruco.drawDetectedCornersCharuco(frame, rightCorners, rightIds)
				except:
					print("nope")
				#frame = cv2.aruco.drawDetectedCornersCharuco(frame,e, cornerColor=(255,0,0),charucoIds=res[1])

				# CHECKERBOARD=(6,9)
				# ret, corners = cv2.findChessboardCorners(gray, CHECKERBOARD, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FAST_CHECK + cv2.CALIB_CB_NORMALIZE_IMAGE)
				# print(type(corners), type(res2), type(res[0]), type(res[1]))
				# frame = cv2.drawChessboardCorners(frame, CHECKERBOARD, corners, ret)
				if rightCorners is not None and rightIds is not None and len(rightCorners)>3:
					allCorners.append(rightCorners)
					allIds.append(rightIds)
		cv2.imshow('frame',frame)
		ert = cv2.waitKey(1)

		if ert & 0xFF == ord('q'):
			break
		decimator+=1

imsize = gray.shape

cap.release()
cv2.destroyAllWindows()

#Calibration fails for lots of reasons. Release the video if we do

l = len(allIds)

r = list(range(len(allIds)))

random.shuffle(r)

newAllCorners = []
newAllIds = []

for ele in r:
	newAllCorners.append(allCorners[ele])
	newAllIds.append(allIds[ele])

print("number of calibs:", l)

num = 150

allCorners = newAllCorners[:min(num,l)]
allIds = newAllIds[:min(num,l)]

print("now it's just", len(allCorners), len(allIds))

print("okay, calbrating....")
retval, cameraMatrix, distCoeffs, rvecs, tvecs = cv2.aruco.calibrateCameraCharuco(allCorners,allIds,board,imsize,None,None)

with open('grids.pickle', 'wb') as f:
		# Pickle the 'data' dictionary using the highest protocol available.
		pickle.dump((retval, cameraMatrix, distCoeffs, rvecs, tvecs), f, pickle.HIGHEST_PROTOCOL)

	
