import time
import cv2
import cv2.aruco
import numpy as np
import random
import multiprocessing
import traceback
import sys

import pickle

# squaresize = .0551
squaresize = .05178
arucosize = squaresize*.9

checkerboardWidth = 9
checkerboardHeight = 6

monitorWidthPx = 2560
monitorHeightPx = 1440
bine = 50

numImagesRequired = 40

dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_50)
board = cv2.aruco.CharucoBoard_create(checkerboardWidth,checkerboardHeight,squaresize,arucosize,dictionary)
img = board.draw((monitorWidthPx,monitorHeightPx-(2*bine)))

img = np.vstack((np.ones((bine,monitorWidthPx)), img, np.ones((bine,monitorWidthPx))))



#Dump the calibration board to a file

cv2.namedWindow("oh yeah", 0)
cv2.moveWindow("oh yeah", 2200, 100)
cv2.setWindowProperty("oh yeah", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

cv2.imshow('oh yeah',img)

#Start capturing images for calibration
cameras = []

for num in [0,2]:
	cap = cv2.VideoCapture(num, cv2.CAP_V4L2)
	cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
	cap.set(cv2.CAP_PROP_FPS,350)
	cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1.0) # 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
	cap.set(cv2.CAP_PROP_EXPOSURE, 60)
	ret,frame = cap.read()
	print(num,ret)
	if ret == True:
		cameras.append(cap)


allCorners = [[],[]]
allIds = [[],[]]

allCornersStereo = [[],[]]

eriam = 0
doAppend = False
while True:
	imgs = []
	for idx, cap in enumerate(cameras):
		ret,frame = cap.read()		
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		res = cv2.aruco.detectMarkers(gray,dictionary)
		mcorners = res[0]
		ids = res[1]
		currentCorners = [[],[]]
		currentIds = [[],[]]
		if len(res[0])>0:
			e = None
			retval, currentCorners[idx], currentIds[idx] = cv2.aruco.interpolateCornersCharuco(mcorners, ids ,gray,board,)
			cv2.aruco.drawDetectedMarkers(frame,res[0],res[1])
			try:
				cv2.aruco.drawDetectedCornersCharuco(frame, currentCorners[idx], currentIds[idx])
			except:
				print("nope")
			if doAppend and currentCorners[idx] is not None and currentCorners[idx] is not None and len(currentCorners[idx])>3:
				print(f"capturing {idx}")
				allCorners[idx].append(currentCorners[idx])
				allIds[idx].append(currentIds[idx])
		imgs.append(frame)
	try:
		if doAppend and (len(allCorners[0][-1]) == 40) and (len(allCorners[1][-1]) == 40):
			print(f"capturing stereo {len(allCornersStereo[0])+1}")
			eriam+=1
			print(eriam)
			allCornersStereo[0].append(allCorners[0][-1])
			allCornersStereo[1].append(allCorners[1][-1])
	except:
		traceback.print_exc(file=sys.stdout)
	sf = 0.65
	shw = cv2.resize(np.vstack(imgs), (0, 0), fx = sf, fy = sf)
	cv2.imshow(f"frame",shw)
	ert = cv2.waitKey(1)
	if ert & 0xFF == ord('q'):
		break
	doAppend = (ert & 0xFF == ord("e"))


	

imsize = gray.shape

cap.release()
cv2.destroyAllWindows()



for n in range(2):
	idsreee = allIds[n]
	cornersreee = allCorners[n]
	l = len(idsreee)
	r = list(range(len(idsreee)))

	random.shuffle(r)

	newAllCorners = []
	newAllIds = []

	for ele in r:
		newAllCorners.append(cornersreee[ele])
		newAllIds.append(idsreee[ele])

	print("number of calibs:", l)

	allCorners[n] = newAllCorners[:min(numImagesRequired,l)]
	allIds[n] = newAllIds[:min(numImagesRequired,l)]

	print("now it's just", len(allCorners[n]), len(allIds[n]))


print("[]", len(allCornersStereo[0][0]))
print("eram", len(allCornersStereo[0]))
# Chessboard parameters
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ...., (checkerboardWidth, checkerboardHeight,0)
objpp = np.zeros(((checkerboardHeight-1)*(checkerboardWidth-1),3), np.float32)
objpp[:,:2] = np.mgrid[0:checkerboardWidth-1,0:checkerboardHeight-1].T.reshape(-1,2)
objpp = objpp * squaresize # Set the Object Points to be in real coordinates
objpp = np.asarray([objpp])
objp = np.copy(objpp)
for x in range(len(allCornersStereo[0])-1):
	objp = np.concatenate((objp, objpp), axis=0)
# print(objp)

# objp = objp
# objp = objp[:len(allCornersStereo[0])]
print(objp.shape)
objp = objp[:, :, None, :]
print(objp.shape)


print(objp, "\n\n\n\n\n                                  lEFT CORNERS", allCornersStereo[0], "\n\n\n\n\n\n\n\nRIGHT                   CORNERS              RIGHT", allCornersStereo[1])
print(len(objp),len(allCornersStereo[0]))

calibration_flags = cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC+cv2.fisheye.CALIB_FIX_SKEW+cv2.fisheye.CALIB_CHECK_COND

def calibrate_fisheye(all_image_points, all_true_points, image_size):
		""" Calibrate a fisheye camera from matching points.
		:param all_image_points: Sequence[Array(N, 2)[float32]] of (x, y) image coordinates of the points.  (see  cv2.findChessboardCorners)
		:param all_true_points: Sequence[Array(N, 3)[float32]] of (x,y,z) points.  (If from a grid, just put (x,y) on a regular grid and z=0)
				Note that each of these sets of points can be in its own reference frame,
		:param image_size: The (size_y, size_x) of the image.
		:return: (rms, mtx, dist, rvecs, tvecs) where
				rms: float - The root-mean-squared error
				mtx: array[3x3] A 3x3 camera intrinsics matrix
				dst: array[4x1] A (4x1) array of distortion coefficients
				rvecs: Sequence[array[N,3,1]] of estimated rotation vectors for each set of true points
				tvecs: Sequence[array[N,3,1]] of estimated translation vectors for each set of true points
		"""
		assert len(all_true_points) == len(all_image_points)
		all_true_points = list(all_true_points)  # Because we'll modify it in place
		all_image_points = list(all_image_points)
		while True:
				assert len(all_true_points) > 0, "There are no valid images from which to calibrate."
				try:
						rms, mtx, dist, rvecs, tvecs = cv2.fisheye.calibrate(
								objectPoints=all_true_points,#[p[None, :, :] for p in all_true_points],
								imagePoints=all_image_points,#[p[:, None, :] for p in all_image_points],
								image_size=image_size,
								K=np.zeros((3, 3)),
								D=np.zeros((4, 1)),
								flags= cv2.fisheye.CALIB_CHECK_COND  + cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC, #+ cv2.fisheye.CALIB_FIX_INTRINSIC, #+ cv2.fisheye.CALIB_FIX_SKEW
								criteria=(cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 90, 1e-3),
						)
						print('Found a calibration based on {} well-conditioned images.'.format(len(all_true_points)))
						return rms, mtx, dist, rvecs, tvecs
				except cv2.error as err:
						try:
								# print("ERROR IS", err, "                  type  ", type(err))
								idx = int(str(err).split('array ')[1][0])  # Parse index of invalid image from error message
								all_true_points.pop(idx)
								all_image_points.pop(idx)
								print("Removed ill-conditioned image {} from the data.  Trying again...".format(idx))
						except IndexError:
								raise err


yeah = {}

retval, cameraMatrix, distCoeffs, rvecs, tvecs = calibrate_fisheye(allCornersStereo[0],objp,imsize)#,None,None, flags = calibration_flags, criteria =  (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 1e-6))
yeah["left"] = {"cameraMatrix": cameraMatrix, "distCoeffs": distCoeffs, "rvecs": rvecs, "tvecs": tvecs}
retval, cameraMatrix, distCoeffs, rvecs, tvecs = calibrate_fisheye(allCornersStereo[1],objp,imsize)#,None,None, flags = calibration_flags, criteria =  (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 1e-6))
yeah["right"] = {"cameraMatrix": cameraMatrix, "distCoeffs": distCoeffs, "rvecs": rvecs, "tvecs": tvecs}


print(len(allCorners[0]))

valid, leftCameraMatrix, leftDistCoeffs, rightCameraMatrix, rightDistCoeffs, leftToRightRot, leftToRightTrans, essentialMat, fundamentalMat = (
 							cv2.stereoCalibrate(objp, allCornersStereo[0], allCornersStereo[1], yeah["left"]["cameraMatrix"], yeah["left"]["distCoeffs"], yeah["right"]["cameraMatrix"], yeah["right"]["distCoeffs"], imsize))

if(valid):
	# Construct the stereo-rectified parameters for display
	R1, R2, P1, P2, Q, validPixROI1, validPixROI2 = cv2.stereoRectify(leftCameraMatrix,  leftDistCoeffs, 
																		rightCameraMatrix, rightDistCoeffs, 
																	 imsize, 
																		leftToRightRot, leftToRightTrans)

	leftUndistortMap = [None, None]
	leftUndistortMap[0], leftUndistortMap[1] = cv2.initUndistortRectifyMap(leftCameraMatrix, leftDistCoeffs, 
																			 R1, P1, imsize, cv2.CV_32FC1)
	rightUndistortMap = [None, None]
	rightUndistortMap[0], rightUndistortMap[1] = cv2.initUndistortRectifyMap(rightCameraMatrix, rightDistCoeffs, 
																			 R2, P2, imsize, cv2.CV_32FC1)
	undistortMap = (leftUndistortMap, rightUndistortMap)

	print("Stereo Calibration Completed!")
	print("Left to Right Rotation Matrix:")
	print(leftToRightRot)
	print("Left to Right Translation:")
	print(leftToRightTrans)
	print("Essential Matrix:")
	print(essentialMat)
	print("Fundamental Matrix:")
	print(fundamentalMat)
	np.savez("cameraCalibration_cv2.npz",
			leftCameraMatrix=yeah["left"]["cameraMatrix"],
			rightCameraMatrix=yeah["right"]["cameraMatrix"],
			leftDistCoeffs=yeah["right"]["distCoeffs"],
			rightDistCoeffs=yeah["left"]["distCoeffs"],
			leftToRightTrans=leftToRightTrans,
			leftToRightRot=leftToRightRot,
			R1=R1,
			R2=R2,
			P1=P1,
			P2=P2,
			baseline=float(leftToRightTrans[0]*-1.0)
			)
	import datetime
	import shutil
	timea = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")
	shutil.copy("cameraCalibration_cv2.npz", f"cameraCalibration_cv2-{timea}.npz")


	
