import time
import cv2
import cv2.aruco
import numpy as np
import time
import traceback
import sys

import pickle
with open('grids.pickle', 'rb') as f:
	retval, cameraMatrix, distCoeffs, rvecs, tvecs = pickle.load(f)

cv2.namedWindow("frame")

#Start capturing images for calibration
cap = cv2.VideoCapture(1, cv2.CAP_V4L2)
cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
cap.set(cv2.CAP_PROP_FPS,350)
cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 3.0) # 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
# cap.set(cv2.CAP_PROP_EXPOSURE, 62.0)

timestart = time.time()
timeend = time.time()

while True:
	timeend=timestart
	timestart = time.time()
	ret,frame = cap.read()
	#gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	if ret:
		cv2.imshow('frame',cv2.undistort(frame, cameraMatrix, distCoeffs))#
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
	else:
		traceback.print_exc(file=sys.stdout)
	print(1/(timestart-timeend))
	

cap.release()
cv2.destroyAllWindows()