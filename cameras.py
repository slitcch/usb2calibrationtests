import cv2
import numpy as np
class MosesCaptureStereo:
	frameWidth = 1280
	frameHeight = 720
	cameras = []
	def __init__(self,indices=(0,2), expval = 57, manexp = True):
		for idx in indices:
			#Start capturing images for calibration
			cap = cv2.VideoCapture(idx, cv2.CAP_V4L2)
			cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
			cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
			cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
			cap.set(cv2.CAP_PROP_FPS,350)
			cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1.0) # 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
			# 70 was too low, power frequency lines.
			cap.set(cv2.CAP_PROP_EXPOSURE, 57)
			ret,frame = cap.read()
			print(ret)
			if ret == True:
				self.cameras.append(cap)
	def read(self):
		ret0, f0  = self.cameras[0].read()
		ret1, f1 = self.cameras[1].read()
		while ((ret0 == False) or (ret1 == False)):
			print("no")
			ret0, f0  = self.cameras[0].read()
			ret1, f1 = self.cameras[1].read()
		f = np.hstack((f1, f0)) 
		return (True, f)