import os
import numpy as np
import cv2
import math
import time

cv2.namedWindow("oh yeah", 0)
cv2.moveWindow("oh yeah", 2200, 100)
cv2.setWindowProperty("oh yeah", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

cv2.imshow('oh yeah',cv2.imread("Chessboard.png"))

cv2.namedWindow('Frame L',0)

# The number of inside corners on the width (long axis) of the checkerboard
checkerboardWidth = 9
# The number of inside corners on the height (short axis) of the checkerboard
checkerboardHeight = 7
# Checkerboard Dims
checkerboardDims = (checkerboardWidth, checkerboardHeight)
# Termination Criteria for the subpixel corner refinement
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

#Initialize the Stereo Camera's feed
frameWidth = 1280
frameHeight = 720
cameras = []
for idx in (2,0):
	#Start capturing images for calibration
	cap = cv2.VideoCapture(idx, cv2.CAP_V4L2)
	cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
	cap.set(cv2.CAP_PROP_FPS,350)
	cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1.0) # 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
	# 70 was too low, power frequency lines.
	cap.set(cv2.CAP_PROP_EXPOSURE, 60)
	ret,frame = cap.read()
	print(idx, ret)
	if ret == True:
		cameras.append(cap)

class MosesCapture:
	cameras = cameras
	def read(self):
		ret0, f0  = self.cameras[0].read()
		ret1, f1 = self.cameras[1].read()
		while ((ret0 == False) or (ret1 == False)):
			print("no")
			ret0, f0  = self.cameras[0].read()
			ret1, f1 = self.cameras[1].read()
		f = cv2.cvtColor( np.hstack((f1, f0)), cv2.COLOR_BGR2GRAY)
		return (True, f)
cap = MosesCapture()


# Turn the Rigel Exposure Up
# os.system(".\LFTool.exe xu set leap 30 "+str(6111)+"L")

# Initialize 3D Visualizer
frameCount = 0
captureNum = 0
key = cv2.waitKey(1)
while (not (key & 0xFF == ord('q'))):
    key = cv2.waitKey(1)
    # Capture frame-by-frame
    newFrame, leftRightImage = cap.read()
    if (newFrame):
      
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Reshape our one-dimensional image into a two-channel side-by-side view of the Rigel's feed
        # frame             = np.reshape(frame, (frameWidth, frameWidth * 2))
        frame_left        = frame[:, :frameWidth]
        frame_right       = frame[:, frameWidth:]
        frame_left_color  = cv2.cvtColor(frame_left , cv2.COLOR_GRAY2BGR)
        frame_right_color = cv2.cvtColor(frame_right, cv2.COLOR_GRAY2BGR)

        # Detect the Chessboard Corners in the Left Image
        leftDetected, leftCorners = cv2.findChessboardCorners(frame_left, checkerboardDims, None, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE + cv2.CALIB_CB_FAST_CHECK)
        if leftDetected:
            #leftCorners = cv2.cornerSubPix(frame_left, leftCorners, checkerboardDims, (-1,-1), criteria)
            cv2.drawChessboardCorners(frame_left_color, checkerboardDims, leftCorners, ret)

        # Detect the Chessboard Corners in the Left Image
        rightDetected, rightCorners = cv2.findChessboardCorners(frame_right, checkerboardDims, None, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE + cv2.CALIB_CB_FAST_CHECK)
        if rightDetected:
            #leftCorners = cv2.cornerSubPix(frame_left, leftCorners, checkerboardDims, (-1,-1), criteria)
            cv2.drawChessboardCorners(frame_right_color, checkerboardDims, rightCorners, ret)

        if key & 0xFF == ord('z') and leftDetected and rightDetected:
            captureNum += 1
            cv2.imwrite("./chessboardImages/leftCapture_" +str(captureNum)+".png", frame_left)
            cv2.imwrite("./chessboardImages/rightCapture_"+str(captureNum)+".png", frame_right)

        # Display the resulting frame
        cv2.imshow('Frame L', np.hstack((frame_left_color,frame_right_color)))
        # cv2.imshow('Frame R', cv2.resize(frame_right_color))

        frameCount = frameCount + 1

# When everything done, release the capture
cv2.destroyAllWindows()